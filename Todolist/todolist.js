let Tasks = [];
let tasksCount = 0;

function getDOMs() {
    const tasksoutput = document.getElementById("tasksoutput");
    const insertasks = document.getElementById("insertasks");
    const title = document.getElementById("title");
    const error = document.getElementById("error");
    start();
}

function start() {
    hideInsertTasks();
    error.innerHTML = '';
}

function hideInsertTasks() {
    insertasks.style.display = 'none';
    error.value = '';
    title.value = '';
    content.value = '';
}

function showInsertTasks() {
    insertasks.style.display = 'block';
}

function insert() {
    if( (title.value != '')) {
        let task = new Task(title.value, tasksCount + 1);
        Tasks.push(task);
        saveTasks();
        title.value = '';
        
        error.style.color = 'black';
        insertasks.style.display = 'none';
    } else {
        error.innerHTML = 'Invaled Task';
        error.style.color = 'var(--red)';
    }
}

let Task = function(title, id) {
    this.title = title;
    this.id = id;
    tasksCount += 1;
}

function saveTasks() {
    for(var i = 0; i < Tasks.length; i++) {
        Tasks[i].id = i + 1;
    }
    localStorage.setItem('Tasks', JSON.stringify(Tasks));
    loadTasks();
}


function loadTasks() {
    let loadedTasks = JSON.parse(localStorage.getItem('Tasks'));
    displayTasks();
}

function clearTasks() {
    Tasks = [];
    localStorage.removeItem('Tasks');
    location.reload();
}

function clearTask(id) {
    var newid = id - 1;
    Tasks.splice(newid, 1);
    saveTasks();
}


function loadTasksARefresh() {
    let loadtasksAR = JSON.parse(localStorage.getItem('Tasks'));
    if((loadtasksAR != null) || (loadtasksAR.length > 0)) {
        tasksCount = loadtasksAR.length;
        for(var i = 0; i < loadtasksAR.length; i++) {
            Tasks.push(loadtasksAR[i]);
        }
    } else {
        tasksCount = 0;
    }
}

function displayTasks() {
    let loadedTasksOP = JSON.parse(localStorage.getItem('Tasks'));
    if((loadedTasksOP != null) || (loadedTasksOP.length > 0)) {
        let output = '';

        output += '<div>';

        for(var a = 0; a < loadedTasksOP.length; a++) {
            output += '<span>';
            output += '<input class="x-btn" type="button" value="X" onclick="clearTask(' + loadedTasksOP[a].id + ');">'
            output += '<h2>' + loadedTasksOP[a].title + '</h2>' ;
           
        
            output +='</span>';
        }

        output += '</div>';                    
        tasksoutput.innerHTML = output;
        
    } else {
        tasksoutput.innerHTML = 'no Tasks';
    }

}
