
document.addEventListener("DOMContentLoaded", function () {
  const loginForm = document.getElementById("login-form");
  const emailInput = document.getElementById("email");
  const passwordInput = document.getElementById("password");
  const showPasswordCheckbox = document.getElementById("show-password");
  const emailError = document.getElementById("email-error");
  const passwordError = document.getElementById("password-error");

  showPasswordCheckbox.addEventListener("change", function () {
    if (showPasswordCheckbox.checked) {
      passwordInput.type = "text";
    } else {
      passwordInput.type = "password";
    }
  });
  loginForm.addEventListener("submit", function (event) {
    let valid = true;
    // Reset previous error messages
    emailError.textContent = "";
    passwordError.textContent = "";
    // Email validation
    if (!emailInput.value.trim()) {
      emailError.textContent = "Email is required";
      valid = false;
    }
    // Password validation
    if (!passwordInput.value.trim()) {
      passwordError.textContent = "Password is required";
      valid = false;
    }
    if (!valid) {
      event.preventDefault();
    }
  });
});




// list of all addEventListener in javascript (i)