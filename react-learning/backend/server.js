const express = require('express');
const app = express();
const cors = require('cors');
app.use(cors());

// const register = require('./componets/register')
const bodyparser = require('body-parser');
// const path = require('path')

app.use(bodyparser.json());
app.use(bodyparser.urlencoded({extended:true}));


app.use('/register',require('./componets/register'));
app.use('/login',require('./componets/login'));
app.use('/delete',require('./componets/delete'));
app.use('/update',require('./componets/update'));
app.use('/fetch',require('./componets/fetch'));


app.listen(3001,(req,res)=>{
    console.log("server is ok")
})