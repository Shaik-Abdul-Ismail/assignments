import React from "react";
import "./card.css";

// import one from "../img/OnePiece.jpg";
function Cards(props) {
  return (
    <>
      <div className="main">
        <div className="card">
          <img src={props.img} className="card-img-top" />
          <div className="card-body">
            <h5 className="card-title">{props.Name}</h5>
            <p className="card-text">
              Some quick example text to build on the card title and make up the
              bulk of the card's content.
            </p>
            <a href="#" className="btn btn-primary">
              {props.details}
            </a>
          </div>
        </div>
      </div>
      
    </>
  );
}

export default Cards;
