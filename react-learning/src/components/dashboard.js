import React, { useState } from 'react';
import { BrowserRouter as Router, Route, Switch, Redirect } from 'react-router-dom';

const App = () => {
  const [loggedIn, setLoggedIn] = useState(false); // State to track login status

  const handleLogin = () => {
    // Implement your authentication logic here
    // Set loggedIn to true if login is successful
  };

  return (
    <Router>
      <Switch>
        <Route path="/login">
          {loggedIn ? (
            <Redirect to="/dashboard" />
          ) : (
            <LoginPage onLogin={handleLogin} />
          )}
        </Route>
        <Route path="/dashboard">
          {loggedIn ? (
            <DashboardPage />
          ) : (
            <Redirect to="/login" />
          )}
        </Route>
        <Redirect from="/" to="/login" />
      </Switch>
    </Router>
  );
};

const LoginPage = ({ onLogin }) => {
  const handleLoginClick = () => {
    // Call your authentication function
    onLogin();
  };

  return (
    <div>
      <h2>Login</h2>
      <button onClick={handleLoginClick}>Login</button>
    </div>
  );
};

const DashboardPage = () => {
  return (
    <div>
      <h2>Dashboard</h2>
      {/* Display your application content here */}
    </div>
  );
};

export default App;
