import React from 'react'
import { useState, useEffect } from 'react';

function Callback() {
    const [count, setCount] = useState(0);

    useEffect(() => {
      const intervalId = setInterval(() => {
        setCount(prevCount => prevCount + 1);
      },1000);
      return () => clearInterval(intervalId);
    }, []); 
  return (
    <div><h1>{count}</h1></div>
  )
}

export default Callback;