// import React, { useState } from 'react';
// import 'bootstrap/dist/css/bootstrap.min.css';
// import { Button, Modal, ModalHeader, ModalBody, ModalFooter } from 'reactstrap';
// import {Label,Form,FormGroup,Input} from 'reactstrap';

// function Update(args) {
//   const [modal, setModal] = useState(false);

//   const toggle = () => setModal(!modal);

//   return (
//     <div>
//       <Button color="danger" onClick={toggle}>
//         Click Me
//       </Button>
//       <Modal isOpen={modal} toggle={toggle} {...args}>
//         <ModalHeader toggle={toggle}>Modal title</ModalHeader>
//         <ModalBody>
//         <Form>
//         <Form>
//   <FormGroup>
//     <Label for="FristName">
//     FristName
//     </Label>
//     <Input
//       id="FristName" // Use a unique ID for each form element
//       name="FristName"
//       placeholder="FristName"
//       type="text"
//       required
//     />
//   </FormGroup>
//   <FormGroup>
//     <Label for="SecondName">
//       SecondName
//     </Label>
//     <Input
//       id="exampleName" // Use a unique ID for each form element
//       name="name"
//       placeholder="Name"
//       type="text"
//       required
//     />
//   </FormGroup>
//   <FormGroup>
//     <Label for="exampleSalary">
//       Salary
//     </Label>
//     <Input
//       id="exampleSalary"
//       name="salary"
//       placeholder="Salary"
//       type="text"
//       required
//     />
//   </FormGroup>
//   <Button type="submit" color='primary'>
//     Update
//   </Button>
//   <Button color="danger"  onClick={toggle}>
//             Cancel
//           </Button>

// </Form>

// </Form>
//         </ModalBody>
//         <ModalFooter>

//         </ModalFooter>
//       </Modal>
//     </div>
//   );
// }

// export default Update;

import React from "react";
import { useState } from "react";

import { Button, Modal, ModalHeader, ModalBody, ModalFooter } from "reactstrap";
import { Form, FormGroup, Label, Input } from "reactstrap";
function Update(props) {
  const { sendingdata } = props;
  const [modal, setModal] = useState(false);
  const [formdata, setFormdata] = useState({
    firstName: "",
    SecondName: "",
    email: "",
    password: "",
  });
  const handleInput = (event) => {
    event.preventDefault();
    const { name, value } = event.target;

    setFormdata({
      ...formdata,
      [name]: value,
    });
    // console.log(formdata);
  };

  const handleSubmit = (event) => {
    event.preventDefault();
    sendingdata(formdata);
    toggle();
  };

  const toggle = () => setModal(!modal);
  return (
    <div>
      <Button color="danger" onClick={toggle}>
        ClickMe
      </Button>
      <Modal isOpen={modal} toggle={toggle}>
        <ModalHeader toggle={toggle}>Modal title</ModalHeader>
        <ModalBody>
          <Form onSubmit={handleSubmit}>
            <FormGroup>
              <Label for="FirstName" hidden>
                First Name
              </Label>
              <Input
                id="id"
                name="FirstName"
                placeholder="First Name"
                type="text"
                value={formdata.id}
                onChange={handleInput}
                required
              />
            </FormGroup>
            <FormGroup>
              <Label for="SecondName" hidden>
                SecondName
              </Label>
              <Input
                id="SecondName"
                name="SecondName"
                placeholder="SecondName"
                type="text"
                value={formdata.SecondName}
                onChange={handleInput}
                required
              />
            </FormGroup>
            <FormGroup>
              <Label for="email" hidden>
                email
              </Label>
              <Input
                id="email"
                name="email"
                placeholder="email"
                type="text"
                onChange={handleInput}
                required
                value={formdata.name}
              />
            </FormGroup>
            <FormGroup>
              <Label for="password" hidden>
                password
              </Label>
              <Input
                id="password"
                name="password"
                placeholder="password"
                type="text"
                onChange={handleInput}
                required
                value={formdata.salary}
              />
            </FormGroup>
            <ModalFooter>
              <Button color="primary" type="submit">
                Submit
              </Button>
              <Button color="secondary" onClick={toggle}>
                Cancel
              </Button>
            </ModalFooter>
          </Form>
        </ModalBody>
      </Modal>
    </div>
  );
}

export default Update;
