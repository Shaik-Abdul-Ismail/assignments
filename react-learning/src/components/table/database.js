import React, { useState, useEffect } from "react";
import axios from "axios";
import "./database.css";
import Update from "./update";

function Database() {
  const [data, setData] = useState([]);
  useEffect(() => {
    fetchData();
  }, []);
  async function fetchData() {
    try {
      const response = await axios.get("http://localhost:3001/fetch");
      setData(response.data);
    } catch (error) {
      console.error("Error fetching data:", error);
    }
  }
  const handleDelete = (firstName) => {
    axios
      .delete(`http://localhost:3001/delete/${firstName}`)
      .then(() => {
        setData((prevData) =>
          prevData.filter((item) => item.firstName !== firstName)
        );
        console.log(`${firstName} deleted.`);
      })
      .catch((error) => {
        console.error("Error deleting data:", error);
      });
  };

  async function sendingdata(formdata) {
    try {
      await axios.put("http://localhost:3001/update", formdata);
      fetchData();
    } catch (error) {
      console.log("Error in updating the data");
    }
  }
  return (
    <div>
      <table>
        <thead>
          <tr>
            <th>firstName</th>
            <th>secondName</th>
            <th>email</th>
            <th>password</th>
            <th>Action</th>
          </tr>
        </thead>
        <tbody>
          {data.map((item) => (
            <tr key={item.firstName}>
              <td>{item.firstName}</td>
              <td>{item.secondName}</td>
              <td>{item.email}</td>
              <td>{item.password}</td>
              <td>
                <button onClick={() => handleDelete(item.firstName)}>
                  Delete
                </button>
                <Update sendingdata={sendingdata} />
              </td>
            </tr>
          ))}
        </tbody>
      </table>
    </div>
  );
}
export default Database;
