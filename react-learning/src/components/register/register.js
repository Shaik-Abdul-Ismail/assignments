import React from "react";
import {  useState } from "react";
import "./register.css";
import { Link } from "react-router-dom";

import axios from "axios";

const Register = () => {
  const [firstName, setfirstName] = useState("");
  const [secondName, setsecondName] = useState("");
  const [email, setemail] = useState("");
  const [password, setpassword] = useState("");

  const handleRegister = async (event) => {
    event.preventDefault();
    setfirstName("");
    setsecondName("");
    setemail("");
    setpassword("");
    
    try {
      const response = await axios.post("http://localhost:3001/register", {
        firstName,
        secondName,
        email,
        password,
      });

      if (response.data.message === "data inserted") {
        alert("Registration failed");
      } else {
        alert("Registration success");
      }
    } catch (error) {
      console.error(error);
    }
    };
    return (
      <>
        <div className="my-login-page">
          <section className="h-100">
            <div className="container h-100">
              <div className="row justify-content-md-center h-100">
                <div className="card-wrapper">
                  {/* <div className="brand">
                  <img src="img/logo.jpg" alt="bootstrap 4 login page" />
                </div> */}
                  <div className="card fat">
                    <div className="card-body">
                      <h4 className="card-title">Register</h4>
                      <form
                        method="POST"
                        className="my-login-validation"
                        novalidate=""
                      >
                        <div className="form-group">
                          <label for="name">First Name</label>
                          <input
                            id="name"
                            type="text"
                            className="form-control size"
                            name="name"
                            value={firstName}
                            onChange={(e) => setfirstName(e.target.value)}
                            required
                            autoFocus
                          />
                          <div className="invalid-feedback">
                            What's your name?
                          </div>
                        </div>
                        <div className="form-group">
                          <label for="name">Last Name</label>
                          <input
                            id="name"
                            type="text"
                            className="form-control size"
                            name="name"
                            value={secondName}
                            onChange={(second)=> setsecondName (second.target.value)}
                            required
                            autoFocus
                          />
                          <div className="invalid-feedback">
                            What's your name?
                          </div>
                        </div>

                        <div className="form-group">
                          <label for="email">E-Mail Address</label>
                          <input
                            id="email"
                            type="email"
                            className="form-control size"
                            name="email"
                            value={email}
                            onChange={(email)=> setemail(email.target.value)}
                            required
                          />
                          <div className="invalid-feedback">
                            Your email is invalid
                          </div>
                        </div>

                        <div className="form-group">
                          <label for="password">Password</label>
                          <input
                            id="password"
                            type="password"
                            className="form-control size"
                            name="password"
                            value={password}
                            onChange={(pass)=> setpassword(pass.target.value)}
                            required
                            data-eye
                          />
                          <div className="invalid-feedback">
                            Password is required
                          </div>
                        </div>

                        <div className="form-group ">
                          <div className="custom-checkbox custom-control two">
                            <input
                              type="checkbox"
                              name="agree"
                              id="agree"
                              className="custom-control-input size"
                              required=""
                            />
                            <label for="agree" className="custom-control-label">
                              I agree to the{" "}
                              <a href="#">Terms and Conditions</a>
                            </label>
                            <div className="invalid-feedback">
                              You must agree with our Terms and Conditions
                            </div>
                          </div>
                        </div>

                        <div className="form-group m-0">
                          <button
                            type="submit"
                            className="btn btn-primary btn-block"
                            onClick={handleRegister}
                          >
                            Register
                          </button>
                        </div>
                        <div className="mt-4 text-center">
                          Already have an account?{" "}
                          <Link to="/Login">Login</Link>
                        </div>
                      </form>
                    </div>
                  </div>
                  {/* <div className="footer">
                  Copyright &copy; 2017 &mdash; Your Company
                </div> */}
                </div>
              </div>
            </div>
          </section>
        </div>
      </>
    );
  };


export default Register;

