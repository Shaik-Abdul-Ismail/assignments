import React from "react";
import { useState } from "react";
import "./login.css";
import { Link } from "react-router-dom";

import axios from "axios";

const Login = () => {
  const [email, setemail] = useState("");
  const [password, setpassword] = useState("");

  const handleRegister = async (event) => {
    event.preventDefault();
    setemail("");
    setpassword("");

    try {
      const response = await axios.get("http://localhost:3001/login", {
        email,
        password,
      });

      if (response.data.message === "data inserted") {
        alert("Registration failed");
      } else {
        alert("Registration success");
      }
    } catch (error) {
      console.error(error);
    }
  };
  return (
    <>
      <div className="container">
        <section className="h-100">
          <div className="container h-100">
            <div className="row justify-content-md-center h-100">
              <div className="card-wrapper">
                
                <div className="card fat king">
                  <div className="card-body bc">
                    <h4 className="card-title">login</h4>
                    <form
                      method="POST"
                      className="my-login-validation"
                      novalidate=""
                    >
                      <div className="form-group">
                        <label for="email">E-Mail Address</label>
                        <input
                          id="email"
                          type="email"
                          className="bc"
                          name="email"
                          value={email}
                          onChange={(email) => setemail(email.target.value)}
                          required
                        />
                        <div className="invalid-feedback">
                          Your email is invalid
                        </div>
                      </div>

                      <div className="form-group">
                        <label for="password">Password</label>
                        <input
                          id="password"
                          type="password"
                          className=" bc"
                          name="password"
                          value={password}
                          onChange={(pass) => setpassword(pass.target.value)}
                          required
                          data-eye
                        />
                        <div className="invalid-feedback">
                          Password is required
                        </div>
                      </div>

                      <div className="form-group m-0">
                     <Link to="/Cards">
                     <button
                          type="submit"
                          className="btn btn-primary btn-block"
                          onClick={handleRegister}
                        >
                          Login
                        </button>
                        </Link>
                      </div>
                      <div className="mt-4 text-center">
                        Already have an account?{" "}
                        <Link to="/register">register</Link>
                      </div>
                    </form>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </section>
      </div>
    </>
  );
};

export default Login;

// import React, { useState } from "react";
// import axios from "axios";
// import "login.css";
// import { useNavigate } from "react-router-dom";

// function Login() {
//   const [Email, setEmail] = useState("");
//   const [Password, setPassword] = useState("");

//   const navigate = useNavigate();
//   const handleRegister = async (event) => {    event.preventDefault();

//     setEmail("");
//     setPassword("");

//     try {
//       const response = await axios.post("http://localhost:3001/login", {
//         Email,
//         Password,
//       });

//       console.log(response.data.message);

//       if (response.data.message === "Authentication failed") {
//         alert("Cannot login ");
//         // navigate("/register");
//       } else {
//         alert("Logged in successfully");
//         navigate("/revealdetails");
//       }
//     } catch (error) {
//       console.error(error);
//     }
//   };

//   return (
//     <div className="container">
//       <div className="heading">
//         <h1>LOGIN & START NOW !!</h1>
//       </div>
//       <div className="total">
//         <form onSubmit={handleRegister}>
//           <div className="form-group">
//             <label htmlFor="Email"> Email :</label>
//             <input
//               className="question"
//               type="text"
//               id="Email"
//               value={Email}
//               onChange={(e) => setEmail(e.target.value)}
//               required
//             />

//             <p></p>

//             <label htmlFor="Email"> Password :</label>
//             <input
//               className="question"
//               type="password"
//               id="Password"
//               value={Password}
//               onChange={(e) => setPassword(e.target.value)}
//               required
//             />
//           </div>
//           <button className=" btn ">login</button>
//         </form>
//       </div>
//     </div>
//   );
// }
// export default Login;

// import React from 'react';
// import "./login.css";
// import { Link } from 'react-router-dom';
// import axios from 'axios';

// const Login = () => {
//   // const handleRegister  = async (event) => {
//     //     event.preventDefault();

//     //     setEmail("");
//     //     setPassword("");

//     //     try {
//     //       const response = await axios.post("http://localhost:3005/login", {
//     //         Email,
//     //         Password,
//     //       });
//   const EventHandler = async (Event)=>{
//     Event.preventDefault("")
//     try{
//       const response = await axios.get("http://localhost:3001/login")
//       console.log(response);
//     }catch(error){
//       console.error(error);

//     }
//   }
//   return (
//     <div className="container">
//       <h1>Please Login</h1>
//       <form>
//         <div className="form-control">
//         {/* <label className="label">Email</label> */}
//           <input type="text" required  placeholder='Email'/>

//         </div>

//         <div className="form-control">
//           <input type="password" required placeholder='Password' />
//           {/* <label className="label">Password</label> */}
//         </div>
//         <button className="btn">Login</button>
//         <p className="text">
//           Don't have an account? <Link to="/Register">Register</Link>
//         </p>
//       </form>
//     </div>
//   );
// };

// export default Login;

// // import { Link } from "react-router-dom";
// // import Register from "../register/register";

// // const Login = ()=>{
// //     const label = document.querySelectorAll('.label');
// //     label.forEach(e =>{
// //         e.innerHTML = e.innerText.split('').map((letter,index)=>{
// //             `<span style = "trasition-delay:${index*50}ms">${letter}</span>`
// //         }).join('')
// //     })
// //     return(
// //         <div className="container">
// //       <h1>Please Login</h1>
// //       <form>
// //         <div className="form-control">
// //           <input type="text" required />
// //           <label className="label">Email</label>
// //            {/* <label>
// //             <span style="transition-delay: 0ms">E</span>
// //               <span style="transition-delay: 50ms">m</span>
// //               <span style="transition-delay: 100ms">a</span>
// //               <span style="transition-delay: 150ms">i</span>
// //               <span style="transition-delay: 200ms">l</span>
// //              </label> */}
// //         </div>

// //         <div className="form-control">
// //           <input type="password" required />
// //           <label className="label">Password</label>
// //         </div>
// //         <button className="btn">Login</button>
// //         <p className="text">Don't have an account? <a href="#"><Link to="/Register">Register</Link></a> </p>
// //       </form>
// //     </div>

// //     )

// // }
// // export default Login
