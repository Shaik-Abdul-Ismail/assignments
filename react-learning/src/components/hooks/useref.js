// import React, { useState } from "react";
// const useref = ()=>{
//     const [name, setName]=useState("");
//     return (
//         <div>
//             <input type = 'text' placeholder="Name" onChange={(e)=>{setName(e.target.value)}} />
//             <text>{name}</text>
//         </div>
//     )
// }
// export default useref

// import React, { useState } from "react";

// const Useref = () => {
//   const [name, setName] = useState("");
//   const username = useRef();
//   const useremail = useRef();
//   const handleFocus = ()=>{
//     console.log(username.current.value)
//     console.log(useremail.current.value)
//   }

//   return (
//     <div>
//         <input type="text" placeholder="email" ref={useremail} />
//       <input
//         type="text"
//         placeholder="Name"
//         useRef = {username}
//         onChange={(e) => {
//           setName(e.target.value);
//         }}
//       />
//       <p>{name}</p> {/* Changed <text> to <p> */}
//       <button onClick={handleFocus}>submit</button>
//     </div>
//   );
// };

// export default Useref;

import React, { useState, useRef } from "react";

const Useref = () => {
  const [name, setName] = useState("");
  const username = useRef();
  const useremail = useRef();

  const handleFocus = () => {
    console.log(username.current.value);
    console.log(useremail.current.value);
  };

  return (
    <div>
      <input type="text" placeholder="email" ref={useremail} />
      <input
        type="text"
        placeholder="Name"
        // {/* Use ref to attach the username ref */}
        onChange={(e) => {
          setName(e.target.value);
        }}
        ref={username}
      />
      <p>{name}</p>
      <button onClick={handleFocus}>submit</button>
    </div>
  );
};

export default Useref;
