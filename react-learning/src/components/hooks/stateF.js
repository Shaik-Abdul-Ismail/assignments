import React, {useState} from 'react'

function StatesFunctional() {
    const[count, setCount]=useState(0);
    const[operation, setOperation]=useState("operation");
   const handleIncrement =()=>
    {
        setCount(count+1);
        setOperation("Increasing ")
    }
    const handleDecrement =()=>
    {
        if(count>0)
        {
        setCount(count-1);
        setOperation(" decreasing  ")
        }
        if(count===0)
        {
            setOperation("cannot decrease")
        }
        
    }
  return (
    <div>
           <h1>{count}</h1>
           <h1>{operation}</h1>
        <button onClick={handleIncrement}> Increment</button>
        <button onClick={handleDecrement}> Decrement</button>
   
  
      
    </div>
  )
}

export default StatesFunctional