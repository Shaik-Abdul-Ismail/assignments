import React, { Component } from 'react'

export default class stateC extends Component {
    constructor(props) {
      super(props)
    
      this.state = {
         count:0,
         names: "operation"
      }
    }
    handleIncrement = ()=>{
        // this.state.count+1,
        this.setState({
            count:this.state.count+1,
            names : "increasing"
        })
       
    }
    handleDecrement = () => {

        this.setState({
            count:this.state.count-1,
            names : "decreasing"
        })
    }
    
  render() {
    return (
      <div>
        <h1>{this.state.names}</h1>
        <h1>{this.state.count}</h1>
        <button onClick={this.handleIncrement}> Increment</button>
        <button onClick={this.handleDecrement}> Decrement</button>
        
      </div>
    )
  }
}
