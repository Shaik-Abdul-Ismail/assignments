import React from "react";
import "./navbar.css";
import { Link } from "react-router-dom";

function Navbar() {
  return (
    <div className="nav navbar">
      <nav className="navbar navbar-expand-lg navbar-dark bg-success">
        <div className="container-fluid">
          <a className="navbar-brand" href="/home">
            Navbar
          </a>
          <button
            className="navbar-toggler"
            type="button"
            data-bs-toggle="collapse"
            data-bs-target="#navbarSupportedContent"
            aria-controls="navbarSupportedContent"
            aria-expanded="false"
            aria-label="Toggle navigation"
          >
            <span className="navbar-toggler-icon"></span>
          </button>
          <div
            className="collapse navbar-collapse menu"
            id="navbarSupportedContent"
          >
            <ul className="navbar-nav me-auto mb-2 mb-lg-0">
              <li className="nav-item">
                <a
                  className="nav-link active text"
                  aria-current="page"
                  href="/"
                >
                  <Link to="/">Home</Link>
                </a>
              </li>
              <li className="nav-item">
                <a
                  className="nav-link active text"
                  aria-current="page"
                  href="/table"
                >
                  <Link to="/table">Table</Link>
                </a>
              </li>
              <li className="nav-item">
                <a
                  className="nav-link active text"
                  aria-current="page"
                  href="/newcards"
                >
                  <Link to="/newcards">Cards</Link>
                </a>
              </li>
              <li className="nav-item">
                <a
                  className="nav-link active text"
                  aria-current="page"
                  href="/ContactUs"
                >
                  <Link to="/ContactUs">ContactUs</Link>
                </a>
              </li>
              <li className="nav-item">
                <a
                  className="nav-link active text"
                  aria-current="page"
                  href="/Login"
                >
                  <Link to="/Login">Login</Link>
                </a>
              </li>
              <li className="nav-item">
                <a
                  className="nav-link active text"
                  aria-current="page"
                  href="/Register"
                >
                  <Link to="/Register">Register</Link>
                </a>
              </li>
            </ul>
          </div>
        </div>
      </nav>
    </div>
  );
}

export default Navbar;
