import React from "react";
// import Update from "./components/table/update";
import Navbar from "./components/nav/navbar";
import Home from "./components/Home/home";
import { BrowserRouter } from "react-router-dom";
import Cards from "./components/card/newcards";
import Table from "./components/table/database";
import Logn from "./components/Login_page/login";
import Register from "./components/register/register";
import Footer from "./components/footer/footer";
import { Route, Routes } from "react-router";
import contactus from "./components/contactus/contactus";

function App() {
  return (
    // <div>
    //  <Update />
    // </div>
    <div>
      <BrowserRouter>
        <Navbar />
        <switch>
          <Routes>
            <Route path="/" Component={Home}></Route>
            <Route path="/table" Component={Table}></Route>
            <Route path="/newcards" Component={Cards}></Route>
            <Route path="/ContactUs" Component={contactus}></Route>
            <Route path="/login" Component={Logn}></Route>
            <Route path="/register" Component={Register}></Route>
            <Route path="/newcards" Component={Cards}></Route>
          </Routes>
        </switch>
        {/* <Footer />  */}
      </BrowserRouter>
    </div>
  );
}

export default App;
